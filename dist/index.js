"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @description Utils that webpack will use in compilation stage.
 */
var _ = __importStar(require("lodash"));
var fs_1 = require("fs");
var path = __importStar(require("path"));
var crypto_1 = require("crypto");
var pkg = require(path.join(process.cwd(), 'package.json'));
/**
 * md5 generator hash function
 */
function md5(str) {
    var md5 = crypto_1.createHash('md5');
    return md5.update(str, 'ascii').digest('hex');
}
/**
 * Update hash of specified file, if the `filePath` is `/home/app.json`
 * the function will match a file in this pattern `/home/app.[hash].json`
 * and try to update its [hash].
 * For file has no `hash` in filename, the function will insert hash to it.
 */
function updateHashFromPath(filePath) {
    filePath = path.resolve(filePath);
    var trans = transaction();
    var parsed = path.parse(filePath);
    var version = getVersionMD5();
    var file = matchHashedFile(parsed.name, String(parsed.ext).replace('.', ''), parsed.dir);
    var newFilename = appendHashToFilename(parsed.name, version);
    trans.commit({
        action: fs_1.renameSync,
        args: [
            path.join(parsed.dir, file),
            path.join(parsed.dir, newFilename + parsed.ext)
        ]
    });
    trans.save();
    return version;
}
/**
 * Update hash of specified manifest file in given path and all icons it links to
 * @param {string} manifestPath path of manifest file
 */
function updateManifestFromPath(manifestPath) {
    // an array contains changes that will be applied
    var trans = transaction();
    var version = getVersionMD5();
    var manifestFile = matchHashedFile('manifest', 'json', manifestPath);
    var manifestDir = path.resolve(manifestPath);
    var manifestFullPath = path.join(manifestDir, manifestFile);
    var updatedManifestName = path.join(manifestDir, appendHashToFilename(manifestFile, version));
    var json = JSON.parse(String(fs_1.readFileSync(manifestFullPath)));
    trans.commit({
        action: fs_1.renameSync,
        args: [manifestFullPath, updatedManifestName]
    });
    // generate new content and filennames for each icon
    json.icons = _.map(json.icons, function (icon) {
        var newPath = appendHashToFilename(icon.src, version);
        trans.commit({
            action: fs_1.renameSync,
            args: [path.join(manifestDir, icon.src), path.join(manifestDir, newPath)]
        });
        icon.src = newPath;
        return icon;
    });
    trans.commit({
        action: fs_1.writeFileSync,
        args: [updatedManifestName, JSON.stringify(json, null, 4)]
    });
    trans.save();
    return version;
}
/**
 * Append hash to the name.
 * @param {string} name Input filename
 * @param {string} hash Hash to be updated
 */
function appendHashToFilename(name, hash) {
    var parsed = path.parse(name);
    return _.get(String(parsed.name).split('.'), 0) + '.' + hash + parsed.ext;
}
/**
 * @description get MD5 hash from `version` in package.json
 */
function getVersionMD5() {
    var hash = md5(pkg.version) || '';
    return String(hash).substr(0, 10);
}
/**
 * Find a file that matches the pattern [name].[hash].[ext]
 * For example: manifest.[hash].json
 * @param {string} name name string
 * @param {string} ext extension string
 * @param {string} location
 */
function matchHashedFile(name, ext, location) {
    var files = fs_1.readdirSync(location).join('|');
    var pat = new RegExp(name + '.?[^.]*.' + ext);
    var manifestFile = _.get(pat.exec(files), 0);
    return manifestFile || '';
}
function transaction() {
    var commits = [];
    return {
        flush: function () {
            commits = [];
        },
        // commit a change
        commit: function (payload) { return commits.push(payload); },
        // apply changes
        save: function () {
            _.each(commits, function (c) {
                c.action.apply(c, c.args);
            });
            return true;
        },
    };
}
module.exports = {
    getVersionMD5: getVersionMD5,
    // update manifest file
    updateManifestFromPath: updateManifestFromPath,
    // update a single file
    updateHashFromPath: updateHashFromPath,
};
//# sourceMappingURL=index.js.map