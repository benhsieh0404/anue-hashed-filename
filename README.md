# @anue/hashed-filename

Package for create hashed filename.

## API

### getVersionMD5():string

Returns md5 hash of project's npm package version.

### updateManifestFromPath(path:string):string

Update manifest file in given `path`, also update icons it points to.

```js

import hashedFilename from '@anue/hashed-filename'

hashedFilename.updateManifestFromPath('./static');

```

### updateHashFromPath(path:string)

Insert/update hash to given `path`, consider the following statment

```js
updateHashFromPath('./static/favicon.ico')
```

which will rename `./static/favicon.icon` to `./static/favicon.[hash].icon`
or simply update `[hash]` in the name.