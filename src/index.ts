/// <reference path="./types/index.d.ts" />

/**
 * @description Utils that webpack will use in compilation stage.
 */
import * as _ from 'lodash';
import { renameSync, writeFileSync, readFileSync } from 'fs';
import * as path from 'path';
import Trasaction from './lib/transaction';
import { matchHashedFile, appendHashToFilename, md5 } from './lib/utils';

const pkg = require(path.join(process.cwd(), 'package.json'));

/**
 * Update hash of specified file, if the `filePath` is `/home/app.json`
 * the function will match a file in this pattern `/home/app.[hash].json`
 * and try to update its [hash].
 * For file has no `hash` in filename, the function will insert hash to it.
 */
function updateHashFromPath(filePath:string):string {
    filePath = path.resolve(filePath);
    const trans = new Trasaction();
    const parsed = path.parse(filePath);
    const version = getVersionMD5();
    const file = matchHashedFile(
        parsed.name, 
        String(parsed.ext).replace('.', ''), 
        parsed.dir
    );
    const newFilename = appendHashToFilename(parsed.name, version);
    trans.commit({
        action: renameSync, 
        args: [
            path.join(parsed.dir, file), 
            path.join(parsed.dir, newFilename + parsed.ext)
        ]
    });
    trans.save();

    return version;
}

/**
 * Update hash of specified manifest file in given path and all icons it links to
 * @param {string} manifestPath path of manifest file
 */
function updateManifestFromPath(manifestPath:string) {
    // an array contains changes that will be applied
    const trans = new Trasaction();
    const version = getVersionMD5();
    const manifestFile = matchHashedFile('manifest', 'json', manifestPath);
    const manifestDir = path.resolve(manifestPath);
    const manifestFullPath = path.join(manifestDir, manifestFile);
    const updatedManifestName = path.join(manifestDir, appendHashToFilename(manifestFile, version));
    const json = JSON.parse(String(readFileSync(manifestFullPath)));

    trans.commit({
        action: renameSync, 
        args: [manifestFullPath, updatedManifestName]
    });
    
    // generate new content and filennames for each icon
    json.icons = _.map(json.icons, icon => {
        const newPath = appendHashToFilename(icon.src, version);

        trans.commit({
            action: renameSync, 
            args: [path.join(manifestDir, icon.src), path.join(manifestDir, newPath)]
        });
        icon.src = newPath;

        return icon;
    });
    trans.commit({
        action: writeFileSync, 
        args: [updatedManifestName, JSON.stringify(json, null, 4)]
    });
    trans.save();

    return version;
}

/**
 * @description get MD5 hash from `version` in package.json
 */
function getVersionMD5():string {
    const hash = md5(pkg.version) || '';
    return String(hash).substr(0, 10);
}


module.exports = {
    getVersionMD5,
    // update manifest file
    updateManifestFromPath,
    // update a single file
    updateHashFromPath,
};