import { readdirSync } from 'fs';
import { createHash } from 'crypto';
import path from 'path';
import _ from 'lodash';

/**
 * md5 generator hash function
 */
function md5(str:string):string {
    const md5 = createHash('md5');
    return md5.update(str, 'ascii').digest('hex');
}

/**
 * Append hash to the name.
 * @param {string} name Input filename
 * @param {string} hash Hash to be updated
 */
function appendHashToFilename(name:string, hash:string) {
    const parsed = path.parse(name);
    return _.get(String(parsed.name).split('.'), 0) + '.' + hash + parsed.ext;
}

/**
 * Find a file that matches the pattern [name].[hash].[ext]
 * For example: manifest.[hash].json
 * @param {string} name name string
 * @param {string} ext extension string
 * @param {string} location
 */
function matchHashedFile(name:string, ext:string, location:string) {
    const files = readdirSync(location).join('|');
    const pat = new RegExp(name + '.?[^.]*.' + ext);
    const manifestFile = _.get(pat.exec(files), 0);
    return manifestFile || '';
}

export {
    matchHashedFile,
    appendHashToFilename,
    md5
}