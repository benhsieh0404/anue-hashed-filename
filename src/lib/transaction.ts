/// <reference path="../types/index.d.ts" />

import * as _ from 'lodash';

export default class Transaction {

    commits:Array<TransactionTask> = [];

    flush = () => {
        this.commits = [];
    }

    commit = (payload:TransactionTask) => {
        this.commits.push(payload);
    }

    save = ():boolean => {
        _.each(this.commits, (c:TransactionTask) => {
            c.action(...c.args);
        });
        return true;
    }

}
